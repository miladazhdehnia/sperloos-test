export default function () {
  return `
    <div class="card">
        <div class="card-drop-down card-border">
          +
        </div>
        <div class="card-drop-down--content card-border">
          <div class="text-uppercase">create boost</div>
              <div class="text-divider">
                <span>date period</span>
              </div>
              <div class="date-picker">
                <div>pick date range:</div>
                <div class="date-picker--range">
                    <div class="text-underline">2020/09/10</div>
                    <span class="text-uppercase">to</span>
                    <div class="text-underline">2020/09/10</div>
                </div>
              </div>
        </div>
        <div class="card-body card-border">
          <div>
            <button class="boost-btn">Boost</button>
          </div>
          <div class="card-statistics text-uppercase">
            <span>linking</span>
            <span class="text-bold">100</span>
          </div>
        </div>
      </div>
    `
}