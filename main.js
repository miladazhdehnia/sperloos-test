import cardElement from "./card.js"
import dropDown from "./drop-down.js"

const cardsData = [1, 2] //sample
const cardsContainer = document.getElementById('cards-container')

let cardsList = '';
cardsData.forEach((id) => cardsList += cardElement(id))
cardsContainer.innerHTML = cardsList

window.onload = (() => {
    dropDown({
        container: cardsContainer,
        getDropdown: (item) => item.children[0]
    })
})