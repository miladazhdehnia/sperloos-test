const addClass = (el, className) => el.classList.add(className)
const removeClass = (el, className) => el.classList.remove(className)

export default function ({
    container = '',
    itemClass = 'card',
    itemDropdownClass = 'card-drop-down',
    activeClass = 'active',
    hoverClass = 'hovered',
    getDropdown = () => null

}) {
    let activeDropdown;
    const isTargetInDropdown = (e) => {
        const path = e.path.filter((el) => el.className !== undefined ? el.className.includes(itemDropdownClass) : false)
        return path.length > 0
    }
    const handleMouseOver = (e) => {
        const item = e.target.closest(`div.${itemClass}`)
        if (item && !getDropdown(item).classList.contains(activeClass)) {
            addClass(getDropdown(item), hoverClass)
            item.addEventListener('mouseleave', handleMouseLeave)
        }
    }

    const handleMouseLeave = (e) => {
        removeClass(getDropdown(e.target), hoverClass)
        e.target.removeEventListener('mouseleave', handleMouseLeave)
    }

    container.addEventListener('mouseover', handleMouseOver)

    function windowClickHandler(e) {
        if (!isTargetInDropdown(e)) toggleDropdown(activeDropdown)
    }

    const toggleDropdown = (selectedDropdown) => {
        if (!selectedDropdown.classList.contains(hoverClass)) return
        if (selectedDropdown.classList.contains(activeClass)) {
            window.removeEventListener('click', windowClickHandler, true)
            removeClass(selectedDropdown, activeClass)
            removeClass(selectedDropdown, hoverClass)
            activeDropdown = ''
            return
        }
        if (selectedDropdown !== activeDropdown) {
            if (activeDropdown) toggleDropdown(activeDropdown)
            addClass(selectedDropdown, activeClass)
            selectedDropdown.offsetParent.removeEventListener('mouseleave', handleMouseLeave)
            window.addEventListener('click', windowClickHandler, true)
            activeDropdown = selectedDropdown
        }

    }

    container.addEventListener('click', (e) => {
        if (e.target.classList.contains(itemDropdownClass)) {
            toggleDropdown(e.target)
        }
    })
}